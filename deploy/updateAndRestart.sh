#!/bin/bash

# any future command that fails will exit the script
set -e

# clone the repo
cd /home/ubuntu/demoproject
git pull origin master
